import 'package:coffee/models/user.dart';
import 'package:coffee/services/database.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthService{

  final FirebaseAuth _auth = FirebaseAuth.instance;


  //create user obj based on Firebase User

  User _userFromFirebaseUser(FirebaseUser user){
    return user != null ? User(uid: user.uid) : null;
  }
 //auth change user stream
//this stream gets user dataype
  Stream<User> get user{
    return _auth.onAuthStateChanged
    //.map((FirebaseUser user) => _userFromFirebaseUser(user)); //take the user object and use it
     .map(_userFromFirebaseUser);
  }

  //sign in anonymous
   Future signInAnonymous() async {
     //to log in our user, making a request to DB
     //try to sign in, if succeed then return user
     try{
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
   } catch(error) {
       print(error.toString());
       return null; //if something fails return null
     }
   }

  //sign in using email & password
  Future signInWithEmailAndPassword(String email, String password) async{
    try{
      //after getting an email and password it will try to create a user
      AuthResult result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    }catch(error){
      print(error.toString());
      return null;
    }
  }

  //register with email & password
  Future registerWithEmailAndPassword(String email, String password) async{
    try{
      //after getting an email and password it will try to create a user
      AuthResult result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;

      //create a document for the user with the uid
      await DataBaseService(uid: user.uid).updateUserData('0', 'new coffee maniac', 100);
      return _userFromFirebaseUser(user);
    }catch(error){
      print(error.toString());
      return null;
    }
  }

  //sign out
 Future signOut() async {
   try {
     return await _auth.signOut();
   }catch (error) {
     print(error.toString());
     return null;
   }
 }
 }