import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coffee/models/maniacs.dart';
import 'package:coffee/models/user.dart';



class DataBaseService{

  final String uid;
  DataBaseService({ this.uid });
  //collection reference
  final CollectionReference coffeeCollection = Firestore.instance.collection('coffee_maniacs');

  Future<void> updateUserData(String sugars, String name, int strength) async{
    return await coffeeCollection.document(uid).setData({
      'sugars': sugars,
      'name': name,
      'strength': strength,
    });
  }

  //maniacs list from snapshot
  List<Maniacs> _maniacsListFromSnapshot(QuerySnapshot snapshot){ //returns original data

    return snapshot.documents.map((doc){
      return Maniacs(
        name:  doc.data['name'] ?? '', //return empty string if the data does not exists
        strength: doc.data['strength'] ?? 0,
        sugars: doc.data['sugars'] ?? '0'

      );
    }).toList(); //converting it to a list
  }


  //user from snapshots

  UserData _userDataFromSnapshot(DocumentSnapshot snapshot){
    return UserData(
      uid: uid,
      name: snapshot.data['name'],
      sugars: snapshot.data['sugars'],
      strength: snapshot.data['strength']
    );
  }

  //get coffee mania stream
 Stream<List<Maniacs>> get maniacs{
    return coffeeCollection.snapshots()
    .map(_maniacsListFromSnapshot);

}

//get user doc stream
Stream<UserData> get userData{
    return coffeeCollection.document(uid).snapshots()
        .map(_userDataFromSnapshot);
}
}