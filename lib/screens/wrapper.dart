import 'package:coffee/screens/home/home.dart';
import 'package:coffee/screens/authenticate/authenticate.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:coffee/models/user.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    //trying to access user
    //import all the packages needed, user & provider
    final user = Provider.of<User>(context);
    print(user);  //printing the user instance

    //return either Home or Authenticate widget
    //checking if the user is logged in or logged out to show the Authenticate or home screen
    if(user == null){
      return Authenticate();   //if we do not have a user
    }else {
      return Home();    //if we are logged in (meaning we have a user)
    }
  }
}
