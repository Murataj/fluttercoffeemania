import 'package:coffee/screens/authenticate/sign_in.dart';
import 'package:flutter/material.dart';
import 'package:coffee/screens/authenticate/register.dart';

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {

  bool showSignIn = true;
  void screenView(){
    setState(() => showSignIn = !showSignIn); //alternated change
  }

  @override
  Widget build(BuildContext context) {

    if (showSignIn)
      {
        return SignIn(screenView: screenView);
      }else {
        return Register(screenView: screenView);
    }
  }
}
