import 'package:coffee/shared/constants.dart';
import 'package:coffee/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:coffee/services/auth.dart';


class SignIn extends StatefulWidget {

  final Function screenView;
  SignIn({this.screenView});   //constructer for the widget


  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  final AuthService _auth = AuthService(); //instance of the AuthService class
  //storing username and password
  final _formKey = GlobalKey<FormState>();
  bool loading = false;  //true when we click to a button

  String email = '';
  String password = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    return loading ? Loading() : Scaffold( //if smth is loading
      backgroundColor: Colors.brown[100],
      appBar: AppBar(
        backgroundColor: Colors.brown[400],
        elevation: 0.0, //removing shadows
        title: Text('Sign in to Coffee Mania'),
        actions: <Widget>[
          FlatButton.icon(
              onPressed: () {
                widget.screenView();
              },
              icon: Icon(Icons.person),
              label: Text('Register'))
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: Form(
          key:  _formKey,
          child: Column(
            children: <Widget>[
              SizedBox(height: 20.0),
              TextFormField(
                    decoration: textInputDecoration.copyWith(hintText: 'Email'),
                  validator: (val) => val.isEmpty ? 'Enter an email' : null ,
                onChanged: (val){
             setState(() => email = val);
                },
              ),
              SizedBox(height: 20.0),
              TextFormField(
                  decoration: textInputDecoration.copyWith(hintText: 'Password'),
                  validator: (val) => val.length < 6 ? 'Enter a password with more than 6 chars' : null ,
                  obscureText: true,   //for password safety
                  onChanged:(val){
                  setState(() => password = val);
                },
              ),

              SizedBox(height: 20.0),
              RaisedButton(
                color: Colors.pink[400],
                child: Text(
                  'Sign in',
                  style: TextStyle(color: Colors.white),
                ),

                //interactaction with Firebase
                onPressed: () async{
                  if(_formKey.currentState.validate()){
                    setState(() => loading = true);
                    dynamic result = await _auth.signInWithEmailAndPassword(email, password);
                 if (result == null) {
                     setState(()
                     {
                       loading = false;
                       error = 'Could not sign in with those credentials';
                         //when smth goes wrong
                    });
                   }
                  }
                }
              ),
              SizedBox(height: 12.0),
              Text(
                error,
               style: TextStyle(color:Colors.red, fontSize: 14.0),
               ),
              ] ,
             ),
        ),
          ),
    );
  }
}
