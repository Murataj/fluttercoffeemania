import 'package:coffee/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:coffee/services/auth.dart';
import 'package:coffee/shared/constants.dart';

class Register extends StatefulWidget {

   final Function screenView;
   Register({this.screenView});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  final AuthService _auth = AuthService();
  //global key to identify our form
  final _formKey = GlobalKey<FormState>();
  bool loading = false;

  String email = '';
  String password = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    return loading ? Loading() : Scaffold(
      backgroundColor: Colors.brown[100],
      appBar: AppBar(
        backgroundColor: Colors.brown[400],
        elevation: 0.0, //removing shadows
        title: Text('Sign Up to Coffee Mania'),
        actions: <Widget>[
          FlatButton.icon(
              onPressed: () {
                widget.screenView();
              },
              icon: Icon(Icons.person),
              label: Text('Sign in'))
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: Form(
          key: _formKey, //keep track of the form
          child: Column(
            children: <Widget>[
              SizedBox(height: 20.0),
              TextFormField(
                decoration: textInputDecoration.copyWith(hintText: 'Email'),

                //is valid if is not empty
                validator: (val) => val.isEmpty ? 'Enter an email' : null ,
                  onChanged: (val){
                    setState(() => email = val);
                  },
              ),
              SizedBox(height: 20.0),
              TextFormField(

                  decoration: textInputDecoration.copyWith(hintText: 'Password'),
                //password must be longer than 6 chars
                  validator: (val) => val.length < 6 ? 'Enter a password with more than 6 chars' : null,
                  obscureText: true,   //for password safety
                  onChanged:(val){
                    setState(() => password = val);

                  },
              ),

              SizedBox(height: 20.0),
              RaisedButton(
                color: Colors.pink[400],
                child: Text(
                  'Register',
                  style: TextStyle(color: Colors.white),
                ),

                //interaction with Firebase
                onPressed: () async{
                  //get the current state of the form, return true if we have a valid form
                  if(_formKey.currentState.validate()){
                    setState(() => loading = true );
                    dynamic result = await _auth.registerWithEmailAndPassword(email, password);
                    if (result == null){
                      //update the error state
                      setState(() {
                        loading = false;
                        error = 'Please supply a valid email!';

                      });

                    }
                  }
                }
              ),
              SizedBox(height: 12.0),
              Text(
                error,
                style: TextStyle(color:Colors.red, fontSize: 14.0),

              ),
            ],
          ),
        ),

      ),
    );
  }
}
