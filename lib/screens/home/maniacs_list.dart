import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:coffee/models/maniacs.dart';
import 'package:coffee/screens/home/maniacs_tile.dart';

class ManiacsList extends StatefulWidget {
  @override
  _ManiacsListState createState() => _ManiacsListState();
}

class _ManiacsListState extends State<ManiacsList> {
  @override
  Widget build(BuildContext context) {

    final maniacs = Provider.of<List<Maniacs>>(context) ?? [];

    return ListView.builder(
      itemCount: maniacs.length,
        itemBuilder: (context, index){
        return ManiacsTile(maniacs: maniacs[index]);
        },
    );
  }
}
