import 'package:coffee/models/user.dart';
import 'package:coffee/services/database.dart';
import 'package:coffee/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:coffee/shared/constants.dart';
import 'package:provider/provider.dart';

class SettingsForm extends StatefulWidget {
  @override
  _SettingsFormState createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {

  final _formKey = GlobalKey<FormState>();
  final List<String> sugars = ['0', '1', '2', '3', '4'];
  final List<int> strengths = [100, 200, 300, 400, 500, 600, 700, 800, 900];

  //form values
  String _currentName;
  String _currentSugars;
  int _currentStrength;

  @override
  Widget build(BuildContext context) {

    User user = Provider.of<User>(context);  //access to the current user that is logged in

    return StreamBuilder<UserData>(
      stream: DataBaseService(uid: user.uid).userData,   //specify the stream added to the database class
      builder: (context, snapshot) {
        //checking if we are getting a data, snapshot refers to it
        if(snapshot.hasData){

          UserData userData = snapshot.data;

          return Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Text(
                    'Update your coffee preferences.',
                    style: TextStyle(fontSize: 18.0),
                  ),
                  SizedBox(height: 20.0),
                  TextFormField(
                      initialValue: userData.name,
                      decoration: textInputDecoration,
                      //is valid if is not empty
                      validator: (val) => val.isEmpty ? 'Please enter a name' : null ,
                      onChanged: (val){ setState(() => _currentName = val);
                      }
                  ),
                  SizedBox(height: 10.0),
                  //dropdown
                  DropdownButtonFormField(
                    decoration: textInputDecoration,
                    value: _currentSugars ?? userData.sugars, //track the selected value, it shows that
                    items: sugars.map((sugar){
                      return DropdownMenuItem(
                        value: sugar,     //the value we store in the database
                        child: Text('$sugars sugars'), //list of the options in the dropdown
                      );
                    }).toList(),
                    onChanged: (val) => setState(() => _currentSugars = val ),
                  ),
                  SizedBox(height: 10.0),
                  //slider
                  Slider (
                    value: (_currentStrength ?? userData.strength).toDouble(),
                    activeColor: Colors.brown[_currentStrength ?? userData.strength], //color of the slider's dot it changes based on the coffee's strength
                    inactiveColor: Colors.brown[_currentStrength ?? userData.strength],
                    min: 100.0,
                    max: 900.0,
                    divisions: 8,   //8 divisions
                    onChanged: (val) => setState(() => _currentStrength = val.round()), //takes a double and converts it to an integer
                  ),

                  RaisedButton(
                    color: Colors.pink[400],
                    child: Text(
                      'Update',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () async {
                      if(_formKey.currentState.validate()){
                        await DataBaseService(uid: user.uid).updateUserData(  //finds the record in the database with the id = uid
                            _currentSugars ?? snapshot.data.sugars,
                            _currentName ?? snapshot.data.name,
                            _currentStrength ?? snapshot.data.strength
                        );
                        Navigator.pop(context); //shot the context from the settings screen
                      }
                    }
                  ),
                ],
              ),
          );
        }else {
          return Loading();
        }
      }
    );
  }
}
