import 'package:coffee/models/maniacs.dart';
import 'package:flutter/material.dart';
import 'package:coffee/services/auth.dart';
import 'package:coffee/services/database.dart';
import 'package:provider/provider.dart';
import 'package:coffee/screens/home/maniacs_list.dart';
import 'package:coffee/screens/home/settings_form.dart';

class Home extends StatelessWidget {

  //having an instance of AuthService class
  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {

    void _showSettings(){
      showModalBottomSheet(context: context, builder: (context){
        return Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 60.0),
          child: Text('bottom sheet'),   //to update preferences
        );
      });
    }

    return StreamProvider<List<Maniacs>>.value(
      value: DataBaseService().maniacs,
      child: Scaffold(
        backgroundColor: Colors.brown[50],
        appBar: AppBar(
          title: Text('Coffee Mania'),
          backgroundColor: Colors.brown[400],
          elevation: 0.0,
          actions: <Widget>[
            //adding a button with an icon
            FlatButton.icon(
                icon: Icon(Icons.person),
                label: Text('Log out'),
                onPressed: () async{
                  await _auth.signOut();
                },
                ),
            FlatButton.icon(
                onPressed: () => _showSettings(),
                icon: Icon(Icons.settings),
                label: Text('Settings'),
            )
          ],
        ),
        body: ManiacsList(),
      ),
    );
  }
}
