import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:coffee/models/maniacs.dart';

class ManiacsTile extends StatelessWidget {

  final Maniacs maniacs;
  ManiacsTile({this.maniacs});
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 8.0),
      child: Card(
        margin: EdgeInsets.fromLTRB(20.0, 6.0, 20.0, 0.0),
        child: ListTile(
          leading: CircleAvatar(
            radius: 25.0,
            backgroundColor: Colors.brown[maniacs.strength], //number for the coffee strength
            backgroundImage: AssetImage('assets/coffee_icon.png'),
          ), //display a circle of color for the coffee strength
          title: Text(maniacs.name),
          subtitle: Text('Takes ${maniacs.sugars} sugar(s)'),
        ),
      ),
    );
        
  }
}
